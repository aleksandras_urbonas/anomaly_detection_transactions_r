cat('# 1. Load Data: Actors\n')

# read data
a_raw <-
  fread(
    '4_raw-data/actors.csv'
    , encoding = "UTF-8"
  )

cat('raw actors data loaded.\n')


# convert to data.frame
a_raw <- data.frame(a_raw)


# save to file
save(
  a_raw
  , file='2_data/a_raw.Rdata'
)
cat('raw actors data saved to file.\n')


cat('Next...\n')