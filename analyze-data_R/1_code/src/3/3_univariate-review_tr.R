cat('# 3. Univariate Review: TR\n')
## Setup ####

# Clean start
# rm(list=ls())

# Set project dir
# setwd('c:/cognizant_ds/analyze-data_R/')

load('2_data/tr_2.Rdata')
cat('tr 2 data loaded.\n')
# load('2_data/tr_3_sample.Rdata')
# cat('tr 3 sample data loaded.\n')


## Review Samples Data ####
summary(tr)

head(tr, 2)

### Check unique
sapply(tr, function(x) round(length(unique(x))/length(x)*100,2))

## Univariate Review ####

n <- names(tr)


(x <- 'Date')
summary(tr[,x])
# sample out only tr from year 2018-
idx <- grep('2018-', tr$Date)
# head(idx)
if(length(idx)>0) {
  tr <- tr[idx,]
  cat(
    length(idx)
    , 'rows kept.\n')
}
t <- table(tr[,x])
barplot(t)



(x <- 'Original.Transaction.Amount')
summary(tr[,x])



(x <- 'Functional.Transaction.Amount')
summary(tr[,x])



# (x <- 'Customer.ID1')
# summary(tr[,x])

## group by month: https://ro-che.info/articles/2017-02-22-group_by_month_r
# tr_M <- tr %>%
#   group_by(
#     c.id=Customer.ID1
#     , month=floor_date(Date, "wee")
#   ) %>%
#   summarize(amount=sum(Original.Transaction.Amount))
# floor_date(tr$Date, "month")
# head(tr_M)

df2 <- tr
library('dplyr')
library('lubridate')

xx <-
  df2 %>%
  group_by(c.id=Customer.ID1,week = day(Date)) %>%
  summarise(value = mean(Original.Transaction.Amount))
