cat('# 5.2 Multivariate outliers:\n')


load('2_data/merged_5.rData')
cat('merged 5 data loaded.\n')


head(d)


# features
x <- c(
  'a.income'
  , 'tr_amount__sum'
  , 'tr_amount__mean'
  , 'tr_amount__count'
)

d0 <- d[,x]

# features for 3D chart
x_3d <- c(
  'a.income'
  , 'tr_amount__sum'
  , 'tr_amount__mean'
)

scatterplot3d::scatterplot3d(
  d0[,x_3d]
  , main="3D Scatterplot"
)

# id outliers using `mvoutlier`
idx <-
  which(
    mvoutlier::aq.plot(
      d0
      , delta=qchisq(
        0.999999
        , df=ncol(d0)
      )
      , quan=0.5
      , alpha=0.00001
    )$outliers
  )
# if any id'ed
if(length(idx)>0) {
  # exclude id'ed
  d <- d[-idx,]
  #inform
  # inform
  cat(
    length(idx)
    , 'or'
    , round(100*nrow(d[idx,x])/nrow(d[,x]), 2)
    , '%'
    , 'outliers removed.\n'
  )
}
# save to file
save(
  d
, file='2_data/merged_5_qc.Rdata'
)
cat('merged 5 qc data saved to file.\n')


cat('Next ...\n')
